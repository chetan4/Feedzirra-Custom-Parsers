module FeedEntryParserTagInterface
  def guid; super rescue @guid || @entry_id; end
  def title; super rescue @title; end
  def url; super rescue @url; end
  def author; super rescue @author; end
  def description; super rescue @summary; end
  def published; super rescue @published; end
  def image; super rescue @image; end
  def source; super rescue @source; end
  def enclosure_url; super rescue @enclosure_url; end
  def enclosure_type; super rescue @enclosure_type; end
  def enclosure_length; super rescue @enclosure_length; end
  def categories; super rescue (@categories.blank? ? [] : @categories); end
  def thumbnails; super rescue (@thumbnails.blank? ? [] : @thumbnails); end
  def contents; super rescue (@contents.blank? ? [] : @contents); end
  def credits; super rescue (@credits.blank? ? [] : @credits); end
  def communities; super rescue (@communities.blank? ? [] : @communities); end
  def comments; super rescue (@commments.blank? ? [] : @comments); end
  def responses; super rescue (@responses.blank? ? [] : @responses); end
  def backlinks; super rescue (@backlinks.blank? ? [] : @backlinks); end
  def prices; super rescue (@prices.blank? ? [] : @prices); end
  def subtitles; super rescue (@subtitles.blank? ? [] : @subtitles); end
  def peerlinks; super rescue (@peerlinks.blank? ? [] : @peerlinks); end
  def restrictions; super rescue (@restrictions.blank? ? [] : @restrictions); end
  def scenes; super rescue (@scenes.blank? ? [] : @scenes); end
  def ratings; super rescue (@ratings.blank? ? [] : @ratings); end
  def hashes; super rescue (@hashes.blank? ? [] : @hashes); end
  def players; super rescue (@players.blank? ? [] : @players); end
  def texts; super rescue (@texts.blank? ? [] : @texts); end
  def rights; super rescue (@rights.blank? ? [] : @rights); end
  def licenses; super rescue (@licenses.blank? ? [] : @licenses); end
  def locations; super rescue (@locations.blank? ? [] : @locations); end
  def embeds; super rescue (@embeds.blank? ? [] : @embeds); end
  def copyrights; super rescue (@copyrights.blank? ? [] : @copyrights); end
end
