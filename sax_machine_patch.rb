module SAXMachine

  def self.included(base)
    base.extend ClassMethods
    base.send(:attr_reader, :unparsed_elements)
    base.send(:define_method, :store_unparsed_elements) do |element, value|
      @unparsed_elements = [] unless @unparsed_elements
      @unparsed_elements << [element, value]
    end
  end

  class SAXHandler < Nokogiri::XML::SAX::Document
    def start_element(name, attrs = [])
      object, config, value = stack.last
      sax_config = object.class.respond_to?(:sax_config) ? object.class.sax_config : nil

      if sax_config
        if collection_config = sax_config.collection_config(name, attrs)
          stack.push [object = collection_config.data_class.new, collection_config, ""]
          object, sax_config, is_collection = object, object.class.sax_config, true
        end
        sax_config.element_configs_for_attribute(name, attrs).each do |ec|
          unless parsed_config?(object, ec)
            object.send(ec.setter, ec.value_from_attrs(attrs))
            mark_as_parsed(object, ec)
          end
        end
        if !collection_config && element_config = sax_config.element_config_for_tag(name, attrs)
          stack.push [element_config.data_class ? element_config.data_class.new : object, element_config, ""]
        else
          begin
            object.store_unparsed_elements(name.to_s, attrs)
#            object.class.elements name.to_sym
#            collection_config = sax_config.collection_config(name, attrs)
#            stack.push [object, collection_config, ""]
          rescue
            p 'BOMBED!' * 200
            puts $!.message
            puts $!.backtrace
          end
        end
      end
    end

    def end_element(name)
      (object, tag_config, _), (element, config, value) = stack[-2..-1]
      return unless stack.size > 1 && config && config.name.to_s == name.to_s

      unless parsed_config?(object, config)
        if config.respond_to?(:accessor)
          object.send(config.accessor) << element
        else
          value = config.data_class ? element : value
          unless value == ""
            object.send(config.setter, value)
          end
          mark_as_parsed(object, config)
        end
      end
      stack.pop
    end
  end

end
