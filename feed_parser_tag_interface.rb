module FeedParserTagInterface
  def title; @title; end
  def url; @url; end
  def feed_url; @feed_url; end
  def author; end
  def block; end
  def image; end
  def explicit; end
  def keywords; end
  def owner_name; end
  def owner_email; end
  def summary; super rescue (@summary || @description); end
  def copyright; end
  def ttl; (@ttl || SiteConfig.default_feed_ttl); end
  def error_code; end
  def rating_count; end
  def rating_total; end
  def rating_avg; end
  def view_count; end
  def viewer_count; end
  def categories; []; end
end
