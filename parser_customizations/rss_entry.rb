require File.join(File.dirname(__FILE__), '..', 'feed_entry_parser_tag_interface.rb')

module Feedzirra
  module Parser
    class RSSEntry
      class RSSEntryEnclosure
        include SAXMachine
        attr_accessor :url
        element :enclosure, :value => :url, :as => :url
        element :enclosure, :value => :length, :as => :length
        element :enclosure, :value => :type, :as => :enclosure_type
        element :enclosure, :value => :duration, :as => :duration
      end

      include SAXMachine
      include FeedUtilities
      include FeedEntryParserTagInterface

      alias_method :guid, :id
      
      elements :enclosure, :as => :enclosures, :class => RSSEntryEnclosure
    end
  end
end