require 'htmlentities'
require File.join(File.dirname(__FILE__), '..', 'feed_parser_tag_interface.rb')

module Feedzirra
  module Parser
    class ITunesRSS

      class ITunesCategory
        include SAXMachine
        include FeedEntryUtilities

        element :'itunes:category', :as => :name, :value => :text
        elements :'itunes:category', :as => :sub_categories, :value => :text

        def name
          HTMLEntities.new.decode(@name)
        end

      end

      include SAXMachine
      include FeedUtilities
      include FeedParserTagInterface

      alias_method :author, :itunes_author
      alias_method :block, :itunes_block
      alias_method :image, :itunes_image
      alias_method :explicit, :itunes_explicit
      alias_method :keywords, :itunes_keywords
      alias_method :summary, :itunes_summary

      element :copyright
      elements :"itunes:category", :as => :itunes_categories, :class => ITunesCategory

      def owner_name
        itunes_owners.first.name rescue ""
      end
      
      def owner_email
        itunes_owners.first.email rescue ""
      end

      def categories
        itunes_categories rescue []
      end

      def copyright
        @copyright
      end

      def image
        itunes_image
      end

    end
  end
end

module Feedzirra
  module Parser
    class ITunesRSSOwner
      include SAXMachine
      include FeedUtilities
    end
  end
end