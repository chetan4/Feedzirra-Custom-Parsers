require File.join(File.dirname(__FILE__), '..', 'feed_parser_tag_interface.rb')


module Feedzirra
  module Parser
    class RSS
      include SAXMachine
      include FeedUtilities
      include FeedParserTagInterface
    end
  end
end


