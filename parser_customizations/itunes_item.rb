require File.join(File.dirname(__FILE__), '..', 'feed_entry_parser_tag_interface.rb')

module Feedzirra
  module Parser
    class ITunesRSSItem
      
      class ITunesEnclosure
        include SAXMachine
        attr_accessor :url
        element :enclosure, :value => :url, :as => :url
        element :enclosure, :value => :length, :as => :length
        element :enclosure, :value => :type, :as => :enclosure_type
        element :enclosure, :value => :duration, :as => :duration
      end

      include SAXMachine
      include FeedUtilities
      include FeedEntryParserTagInterface

      elements :enclosure, :as => :enclosures, :class => ITunesEnclosure
      element :'itunes:image', :as => :itunes_image, :value => :href
      element :'itunes:isClosedCaptioned', :as => :itunes_close_captioned
      
      def author; itunes_author; end

      def explicit?
        ['yes', 'true'].include?(itunes_explicit)
      end

      def block?
        ['yes', 'true'].include?(itunes_block)
      end

      def close_captioned?
        ['yes', 'true'].include?(itunes_close_captioned)
      end

      def summary
        itunes_summary || @summary
      end
      
    end
  end
end