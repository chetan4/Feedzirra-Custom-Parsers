require File.join(File.dirname(__FILE__), '..', 'feed_entry_parser_tag_interface.rb')

module Feedzirra
  module Parser
    class AtomEntry
      include SAXMachine
      include FeedUtilities
      include FeedEntryParserTagInterface

      alias_method :guid, :id

    end
  end
end