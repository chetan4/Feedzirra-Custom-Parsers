module Feedzirra
  module Parser
    class MediaRSSItem < Feedzirra::Parser::RSSEntry

      class Enclosure
        include SAXMachine
        attr_accessor :url
        element :enclosure, :value => :url, :as => :url
        element :enclosure, :value => :length, :as => :length
        element :enclosure, :value => :type, :as => :enclosure_type
        element :enclosure, :value => :duration, :as => :duration
      end

      class Thumbnail
        include SAXMachine
        include FeedUtilities

        element :"media:thumbnail", :as => :url, :value => :url
        element :"media:thumbnail", :as => :width, :value => :width
        element :"media:thumbnail", :as => :height, :value => :height
        element :"media:thumbnail", :as => :time, :value => :time

      end

      class Content
        include SAXMachine
        include FeedUtilities

        element :"media:content", :as => :file_size, :value => :fileSize
        element :"media:content", :as => :content_type, :value => :type
        element :"media:content", :as => :medium, :value => :medium
        element :"media:content", :as => :default, :value => :isDefault
        element :"media:content", :as => :expression, :value => :expression
        element :"media:content", :as => :bitrate, :value => :bitrate
        element :"media:content", :as => :framerate, :value => :framerate
        element :"media:content", :as => :samplingrate, :value => :samplingrate
        element :"media:content", :as => :channels, :value => :channels
        element :"media:content", :as => :duration, :value => :duration
        element :"media:content", :as => :height, :value => :height
        element :"media:content", :as => :width, :value => :width
        element :"media:content", :as => :lang, :value => :lang
        element :"media:content", :as => :url, :value => :url
      end

      class Credit
        include SAXMachine
        include FeedUtilities

        element :'media:credit', :as => :role, :value => :role
        element :'media:credit', :as => :name, :value => :text
      end

      class Comment
        include SAXMachine
        include FeedUtilities

        elements :'media:comment', :as => :comments
      end

      class Response
        include SAXMachine
        include FeedUtilities

        elements :'media:response', :as => :responses
      end

      class BackLink
        include SAXMachine
        include FeedUtilities

        elements :'media:backLink', :as => :backlinks
      end

      class Scene
        include SAXMachine
        include FeedUtilities

        class SceneDetail
          include SAXMachine
          include FeedUtilities

          element :sceneTitle, :as => :title
          element :sceneDescription, :as => :description
          element :sceneStartTime, :as => :start_time
          element :sceneEndTime, :as => :end_time
        end

        elements :'media:scene', :as => :scenes, :class => SceneDetail
      end

      class Price
        include SAXMachine
        include FeedUtilities

        element :'media:price', :as => :price_type, :value => :type
        element :'media:price', :as => :price, :value => :price
        element :'media:price', :as => :currency, :value => :currency
        element :'media:price', :as => :info, :value => :info
      end

      class Subtitle
        include SAXMachine
        include FeedUtilities

        element :'media:subTitle', :as => :subtitle_type, :value => :type
        element :'media:subTitle', :as => :lang, :value => :lang
        element :'media:subTitle', :as => :href, :value => :href
      end

      class PeerLink
        include SAXMachine
        include FeedUtilities

        element :'media:peerLink', :as => :peerlink_type, :value => :type
        element :'media:peerLink', :as => :href, :value => :href
      end

      class Restriction
        include SAXMachine
        include FeedUtilities

        element :'media:restriction', :as => :restriction_type, :value => :type
        element :'media:restriction', :as => :relationship, :value => :relationship
        element :'media:restriction', :as => :value, :value => :text
      end

      class Rating
        include SAXMachine
        include FeedUtilities

        element :'media:rating', :as => :scheme, :value => :scheme
        element :'media:rating', :as => :value, :value => :text
      end

      class Hash
        include SAXMachine
        include FeedUtilities

        element :'media:hash', :as => :algo, :value => :algo
        element :'media:hash', :as => :value, :value => :text
      end

      class Player
        include SAXMachine
        include FeedUtilities

        element :'media:player', :as => :url, :value => :url
        element :'media:player', :as => :height, :value => :height
        element :'media:player', :as => :width, :value => :width
      end

      class Text
        include SAXMachine
        include FeedUtilities

        element :'media:text', :as => :text_type, :value => :type
        element :'media:text', :as => :lang, :value => :lang
        element :'media:text', :as => :start, :value => :start
        element :'media:text', :as => :end, :value => :end
        element :'media:text', :as => :value, :value => :text
      end

      class Right
        include SAXMachine
        include FeedUtilities

        element :'media:rights', :as => :status, :value => :status
      end

      class License
        include SAXMachine
        include FeedUtilities

        element :'media:license', :as => :license_type, :value => :type
        element :'media:license', :as => :href, :value => :href
        element :'media:license', :as => :value, :value => :text
      end

      class Location
        class GeoRSS
          include SAXMachine
          include FeedUtilities

          element :'gml:Point', :as => :point
        end


        include SAXMachine
        include FeedUtilities

        element :'media:location', :as => :description, :value => :description
        element :'media:location', :as => :start, :value => :start
        element :'media:location', :as => :end, :value => :end
        element :'georss:where', :as => :georss, :class => GeoRSS

        def position
          georss.point if georss
        end
      end

      class Embed
        class Param
          include SAXMachine
          include FeedUtilities

          element :'media:param', :as => :name, :value => :name
          element :'media:param', :as => :value, :value => :text
        end

        include SAXMachine
        include FeedUtilities

        element :'media:embed', :as => :url, :value => :url
        element :'media:embed', :as => :height, :value => :height
        element :'media:embed', :as => :width, :value => :width
        elements :'media:param', :as => :params, :class => Param
      end

      class Copyright
        include SAXMachine
        include FeedUtilities

        element :'media:copyright', :as => :url, :value => :url
        element :'media:copyright', :as => :value, :value => :text
        
      end

      class Group
        include SAXMachine
        include FeedUtilities

        elements :"media:thumbnail", :as => :media_thumbnails, :class => Feedzirra::Parser::MediaRSSItem::Thumbnail
      end


      class Community
        include SAXMachine
        include FeedUtilities

        element :'media:starRating', :as => :rating_count, :value => :count
        element :'media:starRating', :as => :rating_average, :value => :average
        element :'media:starRating', :as => :rating_min, :value => :min
        element :'media:starRating', :as => :rating_max, :value => :max

        element :'media:statistics', :as => :views, :value => :views
        element :'media:statistics', :as => :favorites, :value => :favorites

        element :'media:tags', :as => :tags
      end
    end
  end
end
