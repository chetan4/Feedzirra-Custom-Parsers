require File.join(File.dirname(__FILE__), 'media_rss_custom_classes')
require File.join(File.dirname(__FILE__), '..', 'feed_entry_parser_tag_interface')

module Feedzirra
  module Parser
    class MediaRSSItem < Feedzirra::Parser::RSSEntry
      
      include SAXMachine
      include FeedUtilities
      include FeedEntryParserTagInterface

      element :guid
      element :title
      element :link, :as => :url
      element :author
      element :description
      element :pubDate, :as => :published

      elements :enclosure, :as => :enclosures, :class => Enclosure

      element :"media:group", :as => :media_group, :class => Group
      element :'media:content', :as => :content

      elements :'media:community', :as => :media_communities, :class => Community
      element :'media:embed', :as => :player_embed, :value => :url
      
      elements :'media:credit', :as => :media_credits, :class => Credit
      elements :'media:content', :as => :media_contents, :class => Content
      elements :'media:thumbnail', :as => :media_thumbnails, :class => Thumbnail
      element :'media:comments', :as => :media_comments, :class => Comment
      element :'media:responses', :as => :media_responses, :class => Response
      element :'media:backLinks', :as => :media_backlinks, :class => BackLink
      element :'media:scenes', :as => :media_scenes, :class => Scene
      
      elements :'media:price', :as => :media_prices, :class => Price
      elements :'media:subTitle', :as => :media_subtitles, :class => Subtitle
      elements :'media:peerLink', :as => :media_peerlinks, :class => PeerLink
      elements :'media:restriction', :as => :media_restrictions, :class => Restriction
      elements :'media:rating', :as => :media_ratings, :class => Rating
      elements :'media:hash', :as => :media_hashes, :class => Hash
      elements :'media:player', :as => :media_players, :class => Player
      elements :'media:text', :as => :media_texts, :class => Text
      elements :'media:rights', :as => :media_rights, :class => Right
      elements :'media:license', :as => :media_licenses, :class => License
      elements :'media:location', :as => :media_locations, :class => Location
      elements :'media:copyright', :as => :media_copyrights, :class => Copyright
      
      element :'media:keywords', :as => :media_keywords
      element :'media:status', :as => :media_status, :value => :state

      elements :'media:embed', :as => :media_embeds, :class => Embed
      
      
      elements :category, :as => :normal_categories
      elements :'media:category', :as => :media_categories

      def thumbnail
        media_group.media_thumbnail
      end

      def thumbnails
        @media_thumbnails || []
      end

      def contents
        @media_contents || []
      end

      def credits
        @media_credits || []
      end

      def communities
        @media_communities || []
      end

      def comments
        (@media_comments.comments rescue []) || []
      end

      def responses
        (@media_responses.responses rescue []) || []
      end

      def backlinks
        (@media_backlinks.backlinks rescue []) || []
      end

      def prices
        (@media_prices rescue []) || []
      end

      def subtitles
        (@media_subtitles rescue []) || []
      end

      def peerlinks
        (@media_peerlinks rescue []) || []
      end

      def restrictions
        (@media_restrictions rescue []) || []
      end

      def scenes
        (@media_scenes.scenes rescue []) || []
      end

      def ratings
        (@media_ratings rescue []) || []
      end

      def hashes
        (@media_hashes rescue []) || []
      end

      def players
        (@media_players rescue []) || []
      end

      def texts
        (@media_texts rescue []) || []
      end

      def rights
        (@media_rights rescue []) || []
      end

      def licenses
        (@media_licenses rescue []) || []
      end

      def locations
        (@media_locations rescue []) || []
      end

      def embeds
        (@media_embeds rescue []) || []
      end

      def copyrights
        (@media_copyrights rescue []) || []
      end

      def categories
        [*normal_categories].compact + [*media_categories].compact
      end

      def description
        @description
      end

      def guid
        @guid || @link
      end
    end
  end
end
