require File.join(File.dirname(__FILE__), 'media_rss_image')
require File.join(File.dirname(__FILE__), 'media_rss_item')

module Feedzirra
  module Parser
    class MediaRSS < Feedzirra::Parser::RSS

      attr_accessor :feed_url

      include SAXMachine
      include FeedUtilities
      include FeedParserTagInterface

      element :title
      element :link, :as => :url
      element :description, :as => :summary

      element :image, :class => Feedzirra::Parser::MediaRSSImage, :as => :media_image
      elements :item, :as => :entries, :class => Feedzirra::Parser::MediaRSSItem
      
      element :author
      element :'media:keywords', :as => :keywords

      def self.able_to_parse?(xml)
        xml =~ /xmlns:media/
      end

      def image
        media_image.url if media_image
      end

    end
  end
end


