module Feedzirra
  module Parser
    class MediaRSSImage
      include SAXMachine
      include FeedUtilities

      element :url
    end
  end
end